//
//  Response.swift
//  WeatherApplication
//
//  Created by Рустам on 31/8/21.
//

public struct Response: Codable{
    public let
    product: String,
    dataseries: DataSeries
    
    public init(product: String,
                dataseries: DataSeries){
        self.product = product
        self.dataseries = dataseries
    }
}
public struct DataSeries: Codable{
    public let
    timepoint: Int,
    cloudcover: Int,
    seeing: Int,
    transparency: Int,
    rh2m: Int,
    temp2m: Int
    
    public init(timepoint: Int,
                cloudcover: Int,
                seeing: Int,
                transparency: Int,
                rh2m: Int,
                temp2m: Int){
        self.timepoint = timepoint
        self.cloudcover = cloudcover
        self.seeing = seeing
        self.transparency = transparency
        self.rh2m = rh2m
        self.temp2m = temp2m
    }
}
