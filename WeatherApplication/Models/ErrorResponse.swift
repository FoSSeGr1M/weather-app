//
//  ErrorResponse.swift
//  WeatherApplication
//
//  Created by Рустам on 26/8/21.
//

import Foundation

public struct ErrorResponse:Codable{
    public let Message: String?
    public let StatusCode: Int?
    public let errorMessages: [String]?
    
    public func getErrorMessage() -> String? {
        return Message
            ?? errorMessages?.joinToString(" ")
            ?? ((StatusCode != nil && StatusCode != 0) ? "Error: $StatusCode" : nil)
    }
    
    func isUnauthorized() -> Bool {
        return StatusCode == 401
    }
    
    public static func create(_ e: Error) -> ErrorResponse? {
        if let apiError  = e as? RxAlamofireError<String>,
            let jsonString = apiError.body,
            !jsonString.isBlank(),
            let jsonData = jsonString.data(using: .utf8),
            let result = try? JSONDecoder().decode(ErrorResponse.self,from: jsonData){
            return result
        } else if let apiError  = e as? RxAlamofireError<String>,
            !apiError.localizedDescription.isEmpty{
            var localizedDescription = apiError.localizedDescription
            if localizedDescription.contains("The Internet connection appears to be offline"){
                localizedDescription = "check_internet_connection_on_your_phone"
            }
            
            return ErrorResponse(
                Message:localizedDescription,
                StatusCode:apiError.statusCode,
                errorMessages:[]
            )
        } else {
            return nil
        }
    }
}
