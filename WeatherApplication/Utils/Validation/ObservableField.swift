//
//  ObservableField.swift
//  WeatherApplication
//
//  Created by Рустам on 27/8/21.
//

import Foundation
import RxSwift
import RxCocoa

class ObservableFieldForCallback{}

class ObservableField<T>:ObservableFieldForCallback{
    private let
    controlProperty:ControlProperty<T>?,
    getValue:() -> (T)
    private var
    setValue:((T) -> ())?
    init(
        controlProperty:ControlProperty<T>? = nil,
        getValue:@escaping () -> (T),
        setValue:((T) -> ())? = nil
    ){
        self.controlProperty = controlProperty
        self.getValue = getValue
        self.setValue = setValue
        super.init()
        disposable = controlProperty?.subscribeOnMain(onNext: {[weak self] value in
            guard let `self` = self else {return}
            var notEqual = true
            if let svalue = value as? String,
                let soldValue = self.oldValue as? String?{
                notEqual = svalue != soldValue ?? ""
            }
            if let dvalue = value as? Date,
                let doldValue = self.oldValue as? Date?{
                notEqual = dvalue != doldValue
            }
            if notEqual{
                self.callOnPropertyChanged()
            }
            self.oldValue = value
        })
    }
    
    func get() -> T{
        oldValue = getValue()
        return getValue()
    }
    
    func set(_ value:T) {
        if let setValue = setValue{
            setValue(value)
            self.oldValue = value
        }else if let controlProperty = controlProperty{
            controlProperty.onNext(value)
        }
    }
    
    private var oldValue:T?
    private var disposable:Disposable!
    private var callbacks = [OnPropertyChangedCallback]()
    func addOnPropertyChangedCallback(_ callback:OnPropertyChangedCallback){
        callbacks.append(callback)
    }
    func removeOnPropertyChangedCallback(_ callback:OnPropertyChangedCallback){
        let i = callbacks.indexOfFirst {  callback === $0}
        if i != -1{
            callbacks.remove(at: i)
        }
    }
    
    func callOnPropertyChanged(){
        let _ = self.callbacks.map{$0.onPropertyChanged(self as ObservableFieldForCallback)}
    }
    
}
class OnPropertyChangedCallback {
    let onPropertyChanged:(ObservableFieldForCallback)->()
    init(onPropertyChanged:@escaping (ObservableFieldForCallback)->()) {
        self.onPropertyChanged = onPropertyChanged
    }
}
