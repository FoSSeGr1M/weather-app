//
//  NSRegularExpression.swift
//  WeatherApplication
//
//  Created by Рустам on 31/8/21.
//

import Foundation

public extension NSRegularExpression{
    func matches(_ text:String?) -> Bool{
        if let text = text{
            return firstMatch(in: text, range: NSRange(text.startIndex..., in: text)) != nil
        } else {
            return false
        }
    }
    func find(_ text:String) -> String?{
        let matches = self.matches(in: text, range: NSRange(text.startIndex..., in: text))
        let matchesStrings = matches.map {
            String(text[Range($0.range, in: text)!])
        }
        return matchesStrings.first
    }
}
