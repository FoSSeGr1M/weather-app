//
//  Dictionary.swift
//  WeatherApplication
//
//  Created by Рустам on 27/8/21.
//

import Foundation

public extension Dictionary where Value : Hashable {

    func swapKeyValues() -> [Value : Key] {
        assert(Set(self.values).count == self.keys.count, "Values must be unique")
        var newDict = [Value : Key]()
        for (key, value) in self {
            newDict[value] = key
        }
        return newDict
    }
}
