//
//  RxSwift.swift
//  WeatherApplication
//
//  Created by Рустам on 26/8/21.
//

import RxSwift

extension ObservableType{
    public func subscribeOnMain(
        onNext: ((Self.Element) -> Void)?,
        onError: ((Error) -> Void)? = nil,
        onCompleted: (() -> Void)? = nil
    ) -> Disposable{
        return observeOn(MainScheduler.instance).subscribe(onNext: onNext,onError: onError,onCompleted: onCompleted)
    }
}
