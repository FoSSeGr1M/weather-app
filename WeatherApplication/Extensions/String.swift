//
//  String.swift
//  WeatherApplication
//
//  Created by Рустам on 26/8/21.
//

import Foundation

extension String{
    public func isBlank() -> Bool{
        let trimmed = trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmed.isEmpty
    }
    public func toRegex() -> NSRegularExpression{
        return try! NSRegularExpression(pattern:self)
    }
}
