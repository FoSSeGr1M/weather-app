//
//  RxAlamofire.swift
//  WeatherApplication
//
//  Created by Рустам on 27/8/21.
//

import Foundation
import Alamofire
import RxSwift
import RxAlamofire

extension Reactive where Base: DataRequest {
    public func codable<T:Codable>(decoder:JSONDecoder = JSONDecoder()) -> Observable<T>{
        return stringWithBodyInError()
            .flatMap{ response -> Observable<T> in
                return Observable.create{ observer in
                    if T.self == String.self{
                        observer.onNext(response as! T)
                    } else{
                        let response = response.isBlank() ? "{}" : response
                        if
                            let jsonData = response.data(using: .utf8),
                            let result = try? decoder.decode(T.self,from: jsonData){
                            observer.onNext(result)
                        } else {
                            observer.onError(RxAlamofireError<Any>(localizedDescription: "JSON_PARSE_ERROR"))
                        }
                    }
                    return Disposables.create()
                }
        }
    }
}

fileprivate extension Reactive where Base: DataRequest {
    func stringWithBodyInError(encoding: String.Encoding? = nil) -> Observable<String> {
        return resultWithBodyInError(responseSerializer: Base.stringResponseSerializer(encoding: encoding))
    }
    
    
    /**
     Transform a request into an observable of the serialized object.
     
     - parameter queue: The dispatch queue to use.
     - parameter responseSerializer: The the serializer.
     - returns: The observable of `T.SerializedObject` for the created download request.
     */
    func resultWithBodyInError<T: DataResponseSerializerProtocol>(responseSerializer: T, queue: DispatchQueue? = nil)
        -> Observable<T.SerializedObject> {
            return Observable.create { observer in
                let dataRequest = self.validateSuccessfulResponseExt()
                    .response(queue: queue!, responseSerializer: responseSerializer) { (packedResponse) -> Void in
                        Logger.logResponse(packedResponse: packedResponse)
                        switch packedResponse.result {
                        case let .success(result):
                            if let _ = packedResponse.response {
                                observer.on(.next(result))
                                observer.on(.completed)
                            } else {
                                observer.on(.error(RxAlamofireUnknownError))
                            }
                        case let .failure(error):
                            observer.on(.error(RxAlamofireError(error:error,packedResponse:packedResponse)))
                        }
                }
                return Disposables.create {
                    dataRequest.cancel()
                }
            }
    }
    
    /// - returns: A validated request based on the status code
    private func validateSuccessfulResponseExt() -> DataRequest {
        return self.base.validate(statusCode: 200 ..< 300)
    }
}

class RxAlamofireError<T>: NSObject, LocalizedError{
    var localizedDescription: String{
        get{_localizedDescription ?? error?.localizedDescription ?? ""}
    }
    private var _localizedDescription:String?
    private var error:Error?
    let statusCode:Int?
    let body:String?
    
    init(error:Error,packedResponse:DataResponse<T>) {
        self.statusCode = packedResponse.response?.statusCode
        self.error = error
        if let data = packedResponse.data,
            let string = String(data: data, encoding: String.Encoding.utf8){
            body = string
        } else{
            body = nil
        }
    }
    
    init(localizedDescription:String) {
        self._localizedDescription = localizedDescription
        statusCode = nil
        body = nil
    }
    override var description: String {
        get {
            return localizedDescription
        }
    }
    
    var errorDescription: String? {
        get {
            return localizedDescription
        }
    }
}
