//
//  Kotlin.swift
//  WeatherApplication
//
//  Created by Рустам on 27/8/21.
//

import Foundation

//MARK: Primitives
public extension String{
    func toIntOrNull()->Int?{
        return Int(self)
    }
    
    func toDoubleOrNull()->Double?{
        return Double(self)
    }
    
    func toDoubleSafe()->Double{
        return Double(self.replace(",", "").replace(" ", "")) ?? 0
    }
    
    func replace(_ of:String,_ with:String) -> String{
        return replacingOccurrences(of: of,with: with)
    }
    
    var length:Int{
        get{
            return count
        }
    }
}

public extension Int{
    func toString() -> String{
        return String(self)
    }
}

public extension Double{
    func toString() -> String{
        return String(self)
    }
}

//MARK: Optional
public extension Optional where Wrapped == String {
    func isNullOrBlank() -> Bool {
        if let value = self {
            return value.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        }
        return true
    }
    
    func isNullOrEmpty() -> Bool {
        if let value = self {
            return value.isEmpty
        }
        return true
    }
}

//MARK: Pair
public struct Pair<F,S> {
    public let first:F,second:S
    public init(first:F,second:S){
        self.first = first
        self.second = second
    }
}
public struct Triple<F,S,T> {
    public let first:F,second:S,third:T
    public init(first:F,second:S,third:T){
        self.first = first
        self.second = second
        self.third = third
    }
}
public func toDictionary<F, S>(_ pairs:[Pair<F,S>]) -> Dictionary<F, S>{
    var d = Dictionary<F, S>()
    for p in pairs{
        d[p.first] = p.second
    }
    return d
}

//MARK: List
public typealias List = Array
public typealias MutableList = Array

public func listOf<T>(_ items:T...) -> List<T>{
    return items
}

public func mutableListOf<T>(_ items:T...) -> List<T>{
    return items
}

public extension Array where Element == String {
    func joinToString(_ s:String) -> String{
        return joined(separator: s)
    }
}

public extension Array{
    func find(_ predicate: (Element) throws -> Bool) -> Element?{
        return try? first(where: predicate)
    }
    func indexOfFirst(_ predicate: (Element) throws -> Bool) -> Int{
        return (try? firstIndex(where: predicate)) ?? -1
    }
    mutating func add(_ newElement: Element){
        append(newElement)
    }
    mutating func removeAt(_ i:Int){
        remove(at: i)
    }
    func count() -> Int{
        return count
    }
    func firstOrNil() -> Element?{
        return first
    }
    func toMutableList() -> Array<Element>{
        return self
    }
}
