//
//  Api.swift
//  WeatherApplication
//
//  Created by Рустам on 27/8/21.
//

import Foundation
import RxSwift
import UIKit
import Alamofire
import ObjectMapper

open class Api: BaseApi{
    
    override var baseUrl: String {get{Constants.SERVER_BASE_URL}}
    
    //     тип header
    override var encoding:ParameterEncoding{ get{ JSONEncoding.default } }
    
    public func getTemp(
        _ latitude: Double,
        _ longtitude: Double
    ) -> Observable<Response>{
        return createRequest(
            url: "astro.php?lon=\(longtitude)&lat=\(latitude)&unit=metric&output=json"
        )
    }
    
    //MARK: singleton
    private static let shared = Api()
    public static func getInstance() -> Api{
        return shared
    }
}
