//
//  City.swift
//  WeatherApplication
//
//  Created by Рустам on 30/8/21.
//

import Foundation
import RealmSwift

class City: Object {
  dynamic var cityName = ""
  dynamic var latitude = 0.0
  dynamic var longitude = 0.0
  dynamic var temperature = ""
}
