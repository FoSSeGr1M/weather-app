//
//  MapViewController.swift
//  WeatherApplication
//
//  Created by Рустам on 28/8/21.
//

import UIKit
import MapKit
import RxSwift
import CoreLocation

class MapViewController: UIViewController{
    @IBOutlet weak var mapView: MKMapView!
    
    var api:Api {get{ Api.getInstance()}}
    let disposeBag = DisposeBag()
    let showAlertStringBinding = PublishSubject<String>()
    
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 500
    var previousLocation: CLLocation?
    var placeMark: CLPlacemark?
    var latitude: CLLocationDegrees!
    var longitude: CLLocationDegrees!
    var lastAnnotation: MKAnnotation!
    
    var viewModel:MapViewModel!
    
    func showAlert(text: String) {
        showAlertStringBinding.onNext(text)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func currentUserLocation(sender: AnyObject) {
        centerViewOnUserLocation()
    }
    
//    трекинг от текущей локации
    func starttrackingUserLocation() {
        mapView.showsUserLocation = true
        centerViewOnUserLocation()
        locationManager.startUpdatingLocation()
        previousLocation = getCenterLocation(for: mapView)
    }
    
//    координаты текущей локации
    func getCenterLocation(for MapView: MKMapView) -> CLLocation {
        latitude = MapView.centerCoordinate.latitude
        longitude = MapView.centerCoordinate.longitude
        api.getTemp(latitude, longitude)
            .subscribeOnMain(onNext: {[weak self] it in
                self?.showAlert(text: it.dataseries.temp2m.toString())
            }).disposed(by: disposeBag)
        return CLLocation(latitude: latitude, longitude: longitude)
        
    }
//    текущий пользователь
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate{
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func addNewPin() {
        let annotation = CustomAnnotation(coordinate: mapView.centerCoordinate, title: "Город", subtitle: "Температура")
        
        mapView.addAnnotation(annotation)
        lastAnnotation = annotation
    }
    
    @IBAction func addNewPoint(sender: AnyObject) {
      addNewPin()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "NewEntry") {
            let controller = segue.destination as! NewPointViewController
          let annotation = sender as! CustomAnnotation
          controller.annotation = annotation
        }
    }
}

extension MapViewController: CLLocationManagerDelegate{
//    авторизация для доступа к карте
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        starttrackingUserLocation()
    }
}
extension MapViewController: MKMapViewDelegate{
//    цвета линий для маршрутов
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = .blue
        renderer.lineWidth = 4.0
        return renderer
    }
//    выбор точки
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let coordinate = CLLocationCoordinate2D(latitude: 0000, longitude: 0000)
        let location = view.annotation
        self.placeMark = MKPlacemark(coordinate: location?.coordinate ?? coordinate)
       
    }
//    изменение рисунка pin
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.canShowCallout = true
        }
        else {
            anView?.annotation = annotation
        }
        
        let cpa = annotation as! CustomPointAnnotation
        anView?.image = UIImage(named:cpa.imageName)
        
        return anView
    }
}

class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
}

class CustomAnnotation: NSObject, MKAnnotation{
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
      self.coordinate = coordinate
      self.title = title
      self.subtitle = subtitle
    }
}
