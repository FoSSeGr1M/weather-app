//
//  BaseMVVMViewController.swift
//  WeatherApplication
//
//  Created by Рустам on 26/8/21.
//

import Foundation

class BaseMVVMViewController: MBaseViewController {
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        bindData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callOnCreateIfNeeded()
        viewModels.forEach { $0.onResume() }
    }
    
    //MARK: DataBinding
    func bindData(){preconditionFailure("This method must be overridden")}
    
    private var onCreateWasNotCalled = true
    private func callOnCreateIfNeeded() {
        if (onCreateWasNotCalled) {
            viewModels.forEach { $0.onCreate() }
            onCreateWasNotCalled = false
        }
    }
    
    //MARK: Internal
    private var viewModels = [BaseViewModel]()
    
    func getViewModel<T : BaseViewModel> (_ vm:T) -> T {
        bindViewModel(vm)
        return vm
    }
    
    private func bindViewModel(_ viewModel: BaseViewModel) {
        if (!viewModels.contains(where: {$0 === viewModel})){
            viewModels.append(viewModel)
        }
        viewModel.showAlertBinding
            .subscribeOnMain(onNext: {[weak self] e in
                self?.showAlert(withError: e)
            })
            .disposed(by: disposeBag)
        viewModel.showAlertStringBinding
            .subscribeOnMain(onNext: {[weak self] m in
                self?.showAlert(title: nil, message: m)
            })
            .disposed(by: disposeBag)
        viewModel.loadingShown
            .subscribeOnMain(onNext: {[weak self] _ in
                self?.loadingIndicatorStateChanged()
            })
            .disposed(by: disposeBag)
    }
    
    internal func isLoading() -> Bool {
        return viewModels.map { (try? $0.loadingShown.value()) ?? false }.contains(true)
    }
    
    internal func loadingIndicatorStateChanged() {
        let isNeedShowLoading = isLoading()
        if isNeedShowLoading{
            showLoading()
        }else{
            hideLoading()
        }
    }
}
