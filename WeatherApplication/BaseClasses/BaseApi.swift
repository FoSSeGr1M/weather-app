//
//  BaseApi.swift
//  WeatherApplication
//
//  Created by Рустам on 27/8/21.
//

import Foundation
import RxSwift
import Alamofire
import RxAlamofire

open class BaseApi {
    
    var baseUrl:String{ get{ preconditionFailure("baseUrl not implemented") } }
    var headers:HTTPHeaders{ get{ return [:] } }
    var encoding:ParameterEncoding{ get{ URLEncoding.default } }
    
    func createRequest<T:Codable>(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding? = nil) -> Observable<T>{
        var allHeaders:HTTPHeaders? = self.headers
        for (k,v) in headers ?? [:]{
            allHeaders?[k] = v
        }
        if allHeaders?.isEmpty ?? false{
            allHeaders = nil
        }
        
        Logger.logRequest(url: url, parameters: parameters, headers: allHeaders, method: method)
        
        return Alamofire.request(
            (baseUrl + url).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding ?? self.encoding,
            headers: allHeaders)
            .rx
            .codable()
    }
}
