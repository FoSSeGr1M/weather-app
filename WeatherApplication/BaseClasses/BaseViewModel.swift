//
//  BaseViewModel.swift
//  WeatherApplication
//
//  Created by Рустам on 26/8/21.
//

import RxSwift
import RxCocoa

class BaseViewModel{
    //MARK: Binding
    let showAlertBinding = PublishSubject<Error>()
    let showAlertStringBinding = PublishSubject<String>()
    let loadingShown = BehaviorSubject<Bool>(value: false)

    func onCreate() {}
    func onResume() {}
    
    let disposeBag = DisposeBag()
    
    
    func showAlert(_ e: Error) {
        showAlertBinding.onNext(e)
    }

    func showAlert(text: String) {
        showAlertStringBinding.onNext(text)
    }

    func showLoading() {
        loadingShown.onNext(true)
    }

    func hideLoading() {
        loadingShown.onNext(false)
    }
}

extension BaseViewModel{
    var api:Api {get{ Api.getInstance()}}
}
