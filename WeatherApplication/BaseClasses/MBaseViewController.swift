//
//  MBaseViewController.swift
//  WeatherApplication
//
//  Created by Рустам on 26/8/21.
//

import UIKit
import RxSwift
import SVProgressHUD

class BaseVCParams{
    var preferredStatusBarStyle: UIStatusBarStyle {get{return .lightContent}}
    var close:String {get{"close"}}
    var error:String {get{"error"}}
    var tryAgain:String{get{"retry_request"}}
    var errorToString:(Error) -> String?{get{{ErrorResponse.create($0)?.getErrorMessage()}}}
}

class MBaseViewController: UIViewController, BaseViewControllerProtocol {
    //MARK: UI
    var params = BaseVCParams()
    override var preferredStatusBarStyle: UIStatusBarStyle{return .lightContent}
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Logger.logViewDidLoad(self)
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        setEmptyBackButton()
    }
    
    //MARK: Others
    var alertController: UIAlertController?
    let disposeBag = DisposeBag()
    
    deinit {
        Logger.logDeinit(self)
    }
    
    @objc fileprivate func hideKeyboardSelector(){hideKeyboard()}
    
    //MARK: loading
    func showLoading(){
        hideKeyboard()
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    func hideLoading(){
        SVProgressHUD.dismiss()
    }
    
}

class BaseTableViewController: UITableViewController,BaseViewControllerProtocol {
    //MARK: UI
    var params = BaseVCParams()
    override var preferredStatusBarStyle: UIStatusBarStyle{return .lightContent}
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Logger.logViewDidLoad(self)
        setEmptyBackButton()
    }
    
    //MARK: Others
    var alertController: UIAlertController?
    let disposeBag = DisposeBag()
    deinit {
        Logger.logDeinit(self)
    }
    
    //MARK: loading
    func showLoading(){
        hideKeyboard()
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    func hideLoading(){
        SVProgressHUD.dismiss()
    }
}


protocol BaseViewControllerProtocol: class {
    
    var params:BaseVCParams {get}
    
    func setEmptyBackButton()
    
    var disposeBag:DisposeBag {get}
    var alertController: UIAlertController? {get set}
    func showAlert(withError e:Error)
    func showAlert(withError e:Error, refreshAction:(()->())?, canGoBack:Bool)
    //for overriding
    func showLoading()
    func hideLoading()
}

extension BaseViewControllerProtocol where Self: UIViewController{
    
    //MARK: Customizing TabBar
    internal func setEmptyBackButton(){
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
    
    func showAlert(title:String?,message:String?){
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        alertController
            .addAction(UIAlertAction(title: "close",
                                     style: UIAlertAction.Style.default,
                                     handler: {_ in }))
        self.alertController?.dismiss(animated: false, completion: nil)
        self.alertController = alertController
        hideLoading()
        present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(withError e:Error){
        showAlert(withError: e, refreshAction: nil, canGoBack: false)
    }
    func showAlert(withError e:Error, refreshAction:(()->())?, canGoBack:Bool){
        hideKeyboard()
        hideLoading()
        var errorText = params.errorToString(e) ?? ""
        if (errorText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty){
            errorText = e.localizedDescription
        }
        self.alertController?.dismiss(animated: false, completion: nil)
        let alertController = UIAlertController(title: params.error,
                                                message: errorText,
                                                preferredStyle: .alert)
        if let refreshAction = refreshAction{
            alertController
                .addAction(UIAlertAction(title: params.tryAgain,
                                         style: UIAlertAction.Style.default,
                                         handler: {_ in
                                            refreshAction()
                }))
            if canGoBack{
                alertController
                    .addAction(UIAlertAction(title: params.close,
                                             style: UIAlertAction.Style.default,
                                             handler: {[weak self]_ in
                                                self?.dismissVC()
                    }))
            }
        } else {
            alertController
                .addAction(UIAlertAction(title: params.close,
                                         style: UIAlertAction.Style.default,
                                         handler: nil))
        }
        present(alertController, animated: true, completion: nil)
        self.alertController = alertController
    }
    
    
    
    func hideKeyboard(){
        view.endEditing(true)
    }
    
    fileprivate func dismissVC(){
        if let navVc = navigationController{
            navVc.popViewController(animated: true)
        } else{
            dismiss(animated: true, completion: nil)
        }
    }
}


extension MBaseViewController{
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MBaseViewController.hideKeyboardSelector))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
}
